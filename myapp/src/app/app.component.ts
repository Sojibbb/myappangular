import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Technology For Us';
  home = 'Home';
  product = 'Our Products';
  services = 'Our Services';
  us= 'About Us';
  contact = 'Contact Us';
}
